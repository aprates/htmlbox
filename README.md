# HTMLbox 🌐

[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)

`HTMLbox` is a versatile HTML generation framework developed for FatScript, designed to simplify the creation of structured and styled HTML documents programmatically.

## Features

- Create complete HTML pages with headers, footers, and structured content dynamically.
- Utilize reusable abstractions like menus, forms, image grids, and text blocks.
- Easily integrate external CSS for comprehensive design control, with support for an One Dark Theme inspired CSS template provided as a default.

## Installation

To use `HTMLbox`, ensure you have `fry` (^3.4.0), the [FatScript](https://fatscript.org) interpreter installed on your system.

**Use the [chef](https://gitlab.com/fatscript/chef) package manager**:

```bash
chef menuadd fatscript.org
chef include htmlbox
chef restock
```

OR

**Clone clone this repository**:

```bash
git clone https://gitlab.com/aprates/htmlbox.git
```

### Extras

Copy the default [styles.css](styles.css) into the root of your project folder and adjust styles as needed. For best experience out of the box, add the [hermit font](hermit.woff2) to the same folder.

Additionally, if enabling `JsonPost` you will also need [jsonpost.js](jsonpost.js) file.

> most likely you will want to add the those files or provide some alternative implementation for those, but they are not strictly necessary to start using `HTMLbox` core

## Usage

Here's a quick example to generate a simple HTML page:

```
_ <- stock.htmlbox

# Create a HTML document generator instance
doc = Html(title = 'Example Page', useJsonPost = true)

# Generate HTML page
html = doc.page(
  content = compose(
    elements = [
      sectionTitle('Welcome to HTMLbox!')
      bodyText('This is a simple demonstration of generating HTML content.')
    ]
  )
)
```

> the above import `stock.htmlbox` assumes installation via `chef` package manager

## Components Overview

The framework includes various predefined components that can be customized:

- **Text Elements**: Add headings, body-text, preformatted and links.
- **Navigation**: Easily add horizontal menus.
- **Forms**: Generate forms with textboxes, radio buttons, checkboxes, and dropdowns.
- **Image Grids**: Create image grids with optional labels and custom aspect ratio.

### page (html base)

Generates a complete HTML page.

- `bodyContent`: HTML content for the body.
- `headerContent`: (Optional) HTML content for the header.
- `footerContent`: (Optional) HTML content for the footer.

> you can prevent default `headerContent` or `footerContent` by providing `HtmlText()` as optional value for those parameters

### encode

Safely encodes text to prevent Cross-Site Scripting (XSS) by converting special HTML characters into HTML entities.

**Parameter:**

- `text`: Text to be encoded.

**Example Usage:**

```
Html.encode('<script>alert("Hello")</script>')
```

### element

Generic method to create any HTML element programmatically.

**Parameters:**

- `tag`: HTML tag to use.
- `props`: Scope of key-value pairs to use as tag attributes.
- `children`: A list of `HTMLText` (other rendered elements) to be composed and placed within this element.

**Example Usage:**

```
Html.element('span', { class = 'xyz' }, [ Html.encode('My Content') ])
```

### compose

Combines multiple HTML components into a single block.

**Parameters:**

- `elements`: List of `HtmlText` components to combine.

**Example Usage:**

```
compose([ bodyText('Hello, world!'), button('Click me') ])
```

### sectionTitle

Generates a heading element with customizable level and styling options.

**Parameters:**

- `title`: Text content of the heading.
- `level`: (Optional) HTML heading level ranging from 1 `<h1>` to 6 `<h6>`.
- `className`: (Optional) CSS class to apply specific styles to the heading.

> level defaults to `<h2>` (2)

**Example Usage:**

```
sectionTitle('Chapter One', 3, 'chapter-header')
```

### bodyText

Outputs text wrapped in the specified HTML tag:

**Parameters:**

- `text`: Text content to display.
- `alignRight`: Boolean to toggle text alignment.

**Example Usage:**

```
bodyText('Welcome to our website.')
```

### menu

Creates a horizontal navigation menu with selectable items.

**Parameters:**

- `options`: List of `HtmlItem` instances, defining each menu item.
- `selected`: URL of the currently selected menu item, used to highlight the active link.
- `isSubmenu`: Use an inverted color scheme with smaller font.

**Structure of HtmlItem:**

```
HtmlItem = (value: Text, label: Text)
```

**Example Usage:**

```
# Render a menu with 'Section1' of 'Home' item selected
menu([ HtmlItem('/', 'Home'), HtmlItem('/about', 'About') ], '/')
menu([ HtmlItem('/', 'Section1'), HtmlItem('/?section=2', 'Section2') ], '/')
```

### imageGrid

Arranges images in a responsive grid layout with a specified aspect ratio.

**Parameters:**

- `images`: List of `HtmlItem` objects each defining an image.
- `showLabel`: Enable the image label overlay.
- `aspectRatio`: Aspect ratio for the images in the grid, such as '4/3' or '16/9'.

**Example Usage:**

```
imageGrid([ HtmlItem('image1.jpg', 'Image One') ], true, '16/9')
```

### form

Generates a form element, enabling data submission.

**Parameters:**

- `action`: URL where the form data will be submitted.
- `fields`: HTML content (such as inputs and buttons) inside the form.
- `method`: (Optional) HTTP method for submission, defaults to 'POST'.

**Example Usage:**

```
form(
  action = '/add'
  content = [
    textbox('name', 'Name')
    button('Add', 'submit')
  ]
)
```

### Grouped options

Both `dropdown` and `radio` use a `List/HtmlItem` and a `selected` parameter to display their options.

**Example Usages:**

```
radio(
  id = 'gender'
  label = 'Gender:'
  options = [
    HtmlItem('female', 'Female')
    HtmlItem('male', 'Male')
    HtmlItem('other', 'Other')
  ]
  selected = null
)
```

### And more...

- textbox (and variants: numbox, passbox and disabled)
- checkbox
- button
- hidden
- preformatted
- link
- list

Note that `numbox` features a built in compatibility default setting for FatScript Number native type, and `checkbox` is designed to work with FatScript Boolean native type. Also `textbox` has a default pattern to limit the server-side type inference via `recode.fromFormData`.

To avoid the risky and bug-prone type inference required with `FormData`, you can enable `JsonPost` mode, which provides a more modern experience using fetch API through an added JavaScript layer.

For other elements usage examples please checkout [test.fat](test.fat) file.

## Contributing

If you have suggestions for new features or improvements, please open an issue on the [HTMLbox GitLab](https://gitlab.com/aprates/htmlbox/issues) page.

### Donations

If you find `HTMLbox` useful and would like to support its development, you can [Buy me a coffee](https://www.buymeacoffee.com/aprates).

## License

[MIT](LICENSE) © 2024-2025 Antonio Prates

### Acknowledgments

`HTMLbox` also may include resources from the following open-source project:

- [hermit font](https://pcaro.es/hermit/) under OFL license
