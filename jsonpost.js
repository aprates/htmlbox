/**
 * @brief JsonPost handler for FatScript HTMLbox
 * @author Antonio Prates <hello@aprates.dev>
 * @copyright HTMLbox <https://gitlab.com/aprates/htmlbox>
 * License: MIT
 */

function setLoadingJsonPost(element) {
  element.className = "loader";
  element.textContent = " ";
  element.style.color = "var(--tertiary-color)";
  element.style.opacity = 1;
  element.focus();
}

function setFeedbackJsonPost(element, message, isSuccess) {
  element.className = "feedback";
  element.textContent = message;
  if (isSuccess) {
    element.style.color = "var(--primary-color, green)";
    console.info("Success:", message);
  } else {
    element.style.color = "var(--secondary-color, red)";
    console.warn("Failure:", message);
  }
  element.focus();

  const minDisplayTime = 5000;
  const addMsPerChar = 40;
  const displayTime = minDisplayTime + addMsPerChar * message.length;
  setTimeout(() => (element.style.opacity = 0), displayTime);
}

function setupJsonPost(formId) {
  document.addEventListener("DOMContentLoaded", () => {
    const form = document.getElementById(formId);

    if (form) {
      form.addEventListener("submit", (event) => {
        event.preventDefault();

        // Build initial JSON data from FormData
        const formData = new FormData(form);
        const json = Object.fromEntries(formData.entries());

        // Perform data type conversions based on field type
        for (let [key, value] of formData.entries()) {
          const field = form.elements[key];
          if (field.type === "checkbox") {
            json[key] = field.checked;
          } else if (field.type === "number") {
            json[key] = value === "" ? null : Number(value);
          }
        }

        // Communicate with server
        const feedback = document.getElementById(formId + "-feedback");
        setLoadingJsonPost(feedback);
        fetch(form.action, {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify(json),
        })
          .then((response) => {
            if (!response.ok) {
              throw new Error(`HTTP status ${response.status}`);
            }
            return response.json();
          })
          .then((data) => {
            // Set sessionId if present on feedback data
            // (you can usa blank string to logout)
            if (typeof data.setSessionId === "string") {
              localStorage.setItem("sessionId", data.setSessionId);
            }
            // Navigate...
            if (data.redirectTo) {
              window.location.href = data.redirectTo;
              return; // ensure no further code in this page executes
            }

            // ...or show server response message
            setFeedbackJsonPost(feedback, data.message, data.isSuccess);
          })
          .catch((underlyingError) => {
            console.error("JsonPost Error:", underlyingError);
            const userFriendlyMsg = "Failed to submit form. Please try again.";
            setFeedbackJsonPost(feedback, userFriendlyMsg, false);
          });
      });
    }
  });
}
